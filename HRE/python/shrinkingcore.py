# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

fig,ax = plt.subplots(figsize=(4,4))

ax.set_aspect('equal')

rcrs = np.linspace(0,1,100)

plt.plot(1 - rcrs**2, rcrs, color='black', linewidth=1, linestyle='-', label='decreasing particle size')
plt.plot(1 - rcrs**3, rcrs, color='black', linewidth=1, linestyle='--', label='constant particle size')

# Axis labels
plt.xlabel(r'$t / \tau_\text{c}$', fontsize=16)
plt.ylabel(r'$R_\text{c} / R_\text{s}$', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.legend()
plt.tight_layout()
plt.savefig('shrinkingcore.pdf')
