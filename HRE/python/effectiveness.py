# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)

from labellines import labelLine, labelLines

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

fig,ax = plt.subplots()

Ca = np.logspace(-3,0,500)

def eta(c,b):
    return (1-c)**1*np.exp(-20 *(1 / (1 + b * c) - 1))

plt.plot(Ca, eta(Ca, 0.5), linewidth=1, color='black', label=r'$\beta_\text{e} = 0.5$')
plt.plot(Ca, eta(Ca, 0.2), linewidth=1, color='black', label=r'$0.2$')
plt.plot(Ca, eta(Ca, 0.1), linewidth=1, color='black', label=r'$0.1$')
plt.plot(Ca, eta(Ca, 0), linewidth=1, color='black', label=r'$0$')
plt.plot(Ca, eta(Ca, -0.1), linewidth=1, color='black', label=r'$-0.1$')
plt.plot(Ca, eta(Ca, -0.2), linewidth=1, color='black', label=r'$-0.2$')
plt.plot(Ca, eta(Ca, -0.5), linewidth=1, color='black', label=r'$-0.5$')

ax.set_xscale('log')
ax.set_yscale('log')

labelLines(plt.gca().get_lines(),zorder=2, xvals=(0.6,0.14), align=False)


plt.ylim(0.1,100)
plt.xlim(0.001,1)

# Axis labels
plt.xlabel(r'$\text{Ca}$', fontsize=16)
plt.ylabel(r'$\eta_\text{e}$', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.savefig('effectiveness.pdf')
