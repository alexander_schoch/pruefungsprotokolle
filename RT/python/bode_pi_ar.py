# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

fig = plt.figure()
ax = fig.add_subplot(1,1,1)

def PI(s):
    return 1 + 1 / s

omega = np.logspace(-2,2,100)
G_PI = PI(omega * 1j)
AR = abs(G_PI)

plt.plot(omega, AR, linewidth=1, color='black')



# Axis labels
plt.xlabel(r'$\omega$', fontsize=16)
plt.ylabel(r'$\text{AR}$', fontsize=16)

ax.set_xscale('log')
ax.set_yscale('log')

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.savefig('bode_pi_ar.pdf')
