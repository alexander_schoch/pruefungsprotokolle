# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

x = np.linspace(-.5,1.5,100)

plt.plot(x, x**2, linewidth=1, color='black', label=r'$y = x^2$')
plt.plot(x, x, linewidth=1, color='black', linestyle='--', label=r'$y = x$')

plt.scatter([0,1],[0,1],s=10,c='black')

plt.text(0,-.1,r'$(0,0)$')
plt.text(1,.9,r'$(1,1)$')

# Axis labels
plt.xlabel(r'$x$', fontsize=16)
plt.ylabel(r'$y$', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend()
plt.savefig('nullclines.pdf')
