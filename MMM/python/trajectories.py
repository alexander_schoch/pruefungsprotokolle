# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
import scipy
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

plt.scatter([0,1],[0,1],s=10,c='black')


S0 = np.array([0,0])
S1 = np.array([1,1])

def dX_dt(X, t=0):
    return np.array([ X[0]*X[0] - X[1], X[1] - X[0] ])
    #return np.array([ 2 * X[0] - X[1], -X[0] + X[1]])

# Jacobian
def d2X_dt2(X, t=0):
    return np.array([[2 * X[0], -1], [-1, 1]])

J_0 = d2X_dt2(S0)
J_1 = d2X_dt2(S1)

#X0 = np.array([0.2,.2])
#
#X, infodict = scipy.integrate.odeint(dX_dt, X0, t, full_output=True)
#xvals, yvals = X.T

#plt.plot(xvals, yvals)

####
nb_points   = 20

x = np.linspace(-1, 2, nb_points)
y = np.linspace(-1, 2, nb_points)

X1 , Y1  = np.meshgrid(x, y)                       # create a grid
DX1, DY1 = dX_dt([X1, Y1])                      # compute growth rate on the gridt
M = (np.hypot(DX1, DY1))                           # Norm of the growth rate 
M[ M == 0] = 1.                                 # Avoid zero division errors 
DX1 /= M                                        # Normalize each arrows
DY1 /= M
Q = plt.quiver(X1, Y1, DX1, DY1, M, pivot='mid')
####

X0 = np.array([
    #[-.5,-1],
    #[-.29,-1],
    #[-1,0],
    #[1,2],
    [-1,-.5],
    [-1,-.4],
    [.615,.485],
    [.6,.5],
    [.31,.19],
    [.28,.22]
])

#X0 = np.array([
#    #[-.5,-1],
#    #[-.29,-1],
#    #[-1,0],
#    #[1,2],
#    [-1,-.5],
#    [-1,-.4],
#    [.63,.5],
#    [.6,.5],
#    [.2,.2],
#    [.3,.2]
#])


t = np.linspace(0,3,1000)

for x0 in X0:
    X, infodict = scipy.integrate.odeint(dX_dt, x0, t, full_output=True)
    xvals, yvals = X.T
    plt.plot(xvals, yvals, linewidth=1, color='black')

plt.xlim(-1,2)
plt.ylim(-1,2)

# Axis labels
plt.xlabel(r'$x$', fontsize=16)
plt.ylabel(r'$y$', fontsize=16)

plt.text(0,-.1,r'$(0,0)$')
plt.text(1,.9,r'$(1,1)$')

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend()
plt.savefig('trajectories.pdf')
