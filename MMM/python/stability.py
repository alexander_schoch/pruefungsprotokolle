# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

x = np.linspace(-4,4,100)

plt.plot(x, x**2 / 4, linewidth=1, color='black', label=r'$\text{det} = \frac{1}{4}\text{tr}^2$')

plt.scatter([3],[1],s=10,c='black')

plt.text(3,.8,r'$(3,1)$')

# Axis labels
plt.xlabel(r'$\text{tr}$', fontsize=16)
plt.ylabel(r'$\text{det}$', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend()
plt.savefig('stability.pdf')
