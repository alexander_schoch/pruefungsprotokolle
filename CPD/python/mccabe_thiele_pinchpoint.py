# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from scipy.optimize import brentq 
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

def vle(x):
    return x * np.exp(1.1 * (1 - x)**1.1)

x = np.linspace(0,1,100)

xB = 0.2
xD = 0.9
L = 1 
V = 2.96 
Ld = V
Vd = 1
D = V - L
B = Ld - Vd 
F = D + B
z = xD * D / F + xB * B / F

def OL_rectifying(x):
    return L * x  / V + xD * D / V

def OL_stripping(x):
    return Ld * x  / Vd - xB * B / Vd

def find_crosssection(x):
    return OL_stripping(x) - OL_rectifying(x)

def qline(x):
    return (Ld - L) * x / (Vd - V) - F * z / (Vd - V)

cross = brentq(find_crosssection, 0, 1)

# x = y
plt.plot([0,1],[0,1],linewidth=1,color='black', linestyle='--', label=r'$x_i = y_i$')

# EL
plt.plot(x, vle(x), linewidth=1, color='darkred', label=r'EL')

# OL
plt.plot([xB, cross], OL_stripping(np.array([xB, cross])), color='darkblue', linewidth=1, label=r'OL, stripping')
plt.plot([cross, xD], OL_rectifying(np.array([cross, xD])), color='darkgreen', linewidth=1, label=r'OL, rectifying')

# q
plt.plot([cross, z], qline(np.array([cross, z])), color='grey', linewidth=1, label=r'q-line')

# Axis labels
plt.xlabel(r'$x_i$', fontsize=16)
plt.ylabel(r'$y_i$', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.legend()
plt.tight_layout()
plt.savefig('mccabe_thiele_pinchpoint.pdf')
