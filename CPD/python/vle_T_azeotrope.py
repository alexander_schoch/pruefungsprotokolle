# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.optimize import brentq
from scipy.stats import t
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

x = np.linspace(0,1,101)

T = [109.32,104.83,102.39,100.93,99.98,99.32,98.84,98.49,98.21,97.99,97.81,97.66,97.53,97.42,97.32,97.24,97.16,97.09,97.03,96.97,96.91,96.86,96.81,96.77,96.73,96.69,96.65,96.61,96.58,96.55,96.52,96.49,96.46,96.44,96.42,96.39,96.38,96.36,96.34,96.33,96.32,96.31,96.30,96.29,96.29,96.29,96.29,96.29,96.29,96.30,96.31,96.33,96.34,96.36,96.39,96.41,96.44,96.47,96.51,96.55,96.60,96.65,96.70,96.76,96.82,96.89,96.96,97.04,97.13,97.22,97.32,97.42,97.53,97.65,97.78,97.92,98.06,98.22,98.38,98.56,98.74,98.94,99.15,99.37,99.61,99.86,100.12,100.41,100.71,101.02,101.36,101.72,102.09,102.50,102.92,103.37,103.85,104.36,104.90,105.47,106.07]

dew = [0.000,0.150,0.224,0.268,0.295,0.314,0.328,0.339,0.347,0.354,0.360,0.364,0.369,0.372,0.376,0.379,0.382,0.384,0.387,0.390,0.392,0.394,0.397,0.399,0.401,0.403,0.405,0.408,0.410,0.412,0.414,0.416,0.419,0.421,0.423,0.426,0.428,0.430,0.433,0.435,0.438,0.441,0.443,0.446,0.449,0.452,0.455,0.458,0.461,0.464,0.467,0.470,0.474,0.477,0.481,0.485,0.489,0.493,0.497,0.501,0.505,0.510,0.515,0.519,0.524,0.530,0.535,0.541,0.546,0.552,0.559,0.565,0.572,0.579,0.586,0.594,0.602,0.611,0.619,0.629,0.638,0.648,0.659,0.670,0.682,0.694,0.707,0.721,0.735,0.751,0.767,0.784,0.802,0.822,0.843,0.865,0.888,0.913,0.940,0.969,1.000]

plt.plot(x, T, linewidth=1, color='black', label=r'bubble line')
plt.plot(dew, T, linewidth=1, color='black', label=r'dew line', linestyle='--')


# Axis labels
plt.xlabel(r'$x_i, y_i$', fontsize=16)
plt.ylabel(r'$\theta$ / \si{\celsius}', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.legend()
plt.tight_layout()
plt.savefig('vle_T_azeotrope.pdf')
