# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)

#plt.style.use('dark_background')

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')


circle1 = plt.Circle((-1, 0), 1, color='r', hatch='//', alpha=0.5, label=r'explicit euler stability region')
circle3 = plt.Circle((0, 0), 4, color='lightblue', hatch='\\', alpha=0.5, label=r'implicit euler stability region')
circle2 = plt.Circle((1, 0), 1, color='white', hatch='\\', alpha=1)


fig, ax = plt.subplots()

plt.xlim(-2.5,2.5)
plt.ylim(-2.5,2.5)

ax.set_aspect(1)

ax.add_artist(circle1)
ax.add_artist(circle3)
ax.add_artist(circle2)

# Axis labels
plt.xlabel(r'$\mathrm{Re}$', fontsize=16)
plt.ylabel(r'$\mathrm{Im}$', fontsize=16)


# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
ax.legend((circle1,circle3), ('explicit euler stability region', 'implicit euler stability region'))
plt.savefig('stability.pdf')
