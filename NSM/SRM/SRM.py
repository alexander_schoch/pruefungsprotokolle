# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)

#plt.style.use('dark_background')

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

X = [10,12,13]
Y = [15,10,14]

QX = [1,1,-1,-1,0]
QY = [1,-1,-1,1,0]

# Axis labels
plt.xlabel(r'Wirkstoff 1', fontsize=16)
plt.ylabel(r'Wirkstoff 2', fontsize=16)

plt.scatter(X,Y, c='black')
plt.scatter(QX,QY,c='black')

def func(x,a):
    return a * x

parms, cov = curve_fit(func, X, Y)

for i in np.arange(0,np.size(QX)):
    shiftx = 0.5
    shifty = -0.5
    txt = r'$(' + str(QX[i]) + ',' + str(QY[i]) + ')$'
    plt.text(QX[i] - shiftx, QY[i] - shifty, txt)

xarr = np.arange(0,max(X),0.01)
plt.plot(xarr, func(xarr, *parms), color='black', linewidth=1)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.savefig('SRM.pdf')
