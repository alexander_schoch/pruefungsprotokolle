# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as lines
import matplotlib.animation as animation
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)
from sympy import *

#plt.style.use('dark_background')

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

def draw_brace(ax, xspan, text):
    """Draws an annotated brace on the axes."""
    xmin, xmax = xspan
    xspan = xmax - xmin
    ax_xmin, ax_xmax = ax.get_xlim()
    xax_span = ax_xmax - ax_xmin
    ymin, ymax = ax.get_ylim()
    yspan = ymax - ymin
    resolution = int(xspan/xax_span*100)*2+1 # guaranteed uneven
    beta = 300./xax_span # the higher this is, the smaller the radius

    x = np.linspace(xmin, xmax, resolution)
    x_half = x[:resolution/2+1]
    y_half_brace = (1/(1.+np.exp(-beta*(x_half-x_half[0])))
                    + 1/(1.+np.exp(-beta*(x_half-x_half[-1]))))
    y = np.concatenate((y_half_brace, y_half_brace[-2::-1]))
    y = ymin + (.05*y - .01)*yspan # adjust vertical position

    ax.autoscale(False)
    ax.plot(x, y, color='black', lw=1)

    ax.text((xmax+xmin)/2., ymin+.07*yspan, text, ha='center', va='bottom')

#########
# input #
#########
EXPLICIT = False
interval = [-2.0,3.0]
NUM_POINTS = 6 

# Cf' = x, f(x0) = 1
x = Symbol('x')
f = Function('f')
eq = Eq(f(x).diff(x),-2*x)
func = dsolve(eq, f(x), ics={f(interval[0]):1.0})

x0 = interval[0] 
y0 = 1 
xmax = interval[1]
h = (xmax - x0) / (NUM_POINTS - 1) 
xarr = np.linspace(x0, x0, NUM_POINTS)
yarr = np.linspace(y0, y0, NUM_POINTS)
dyarr = np.linspace(x0, x0, NUM_POINTS)
xarr[0] = x0
yarr[0] = y0
dyarr[0] = -2 * xarr[0]

if EXPLICIT == True:
    for i in np.arange(1,xarr.size):
       xarr[i] = xarr[i-1] + h
       yarr[i] = yarr[i-1] + h * dyarr[i-1]
       dyarr[i] = -2 * xarr[i]
else:
    for i in np.arange(1,xarr.size):
        xarr[i] = xarr[i-1] + h
        dyarr[i] = -2 * (xarr[i] + h)
        yarr[i] = yarr[i-1] + h * dyarr[i]


array = np.linspace(xarr[0], xarr[-1], 100)
tmp = [func.subs({x: i}).rhs for i in array]
plt.plot(array, tmp, linewidth=1, linestyle='-', label=r'Analytical Solution', color='black')
plt.plot(xarr, yarr, linewidth=1, linestyle='-.', label=r"Euler's Solution", color='black')

GTE = tmp[-1] - yarr[-1]
print('GTE = ' + str(GTE) + ' for h = ' + str(h))





# Axis labels
plt.xlabel(r'$t$', fontsize=16)
plt.ylabel(r'$y$', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.legend()
if EXPLICIT == True:
    plt.arrow(3,-3.9,0,4.8, color='red',linewidth=2, capstyle='projecting')
    plt.text(-0.95,4.6,r'LTE',color='red')
    plt.arrow(-1,4,0,yarr[1] - 4.1, color='red',linewidth=2)
    plt.text(2.65,-1,r'GTE',color='red')
    #plt.arrow(1,1,1,1)
    plt.savefig('forward.pdf')
else:
    plt.savefig('backward.pdf')
