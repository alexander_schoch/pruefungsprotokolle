# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)
from sklearn import *
from sklearn.decomposition import PCA
import pandas as pd
from pandas.plotting import scatter_matrix
import random

#plt.style.use('dark_background')

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

#fig, axis = plt.subplots()

isPCA = False

NUM_POINTS = 200
RAND_INT1 = 1 
RAND_INT2 = 0.4

SCALE_B = 0.5
SCALE_C = 0.1

if isPCA == True:
    dataset_A = np.array([(random.random() * 2 - 1) for i in np.arange(0,NUM_POINTS)]) 
    dataset_B = np.array([(random.random() * 2 - 1) * SCALE_B for i in np.arange(0,NUM_POINTS)]) 
    dataset_C = np.array([(random.random() * 2 - 1) * SCALE_C for i in np.arange(0,NUM_POINTS)]) 
else:
    dataset_A = np.array([(random.random() * 2 - 1) for i in np.arange(0,NUM_POINTS)]) 
    dataset_B = np.array([(random.random() * 2 - 1) for i in np.arange(0,np.size(dataset_A))])
    dataset_C = np.array([-dataset_B[i] + random.random()*RAND_INT2 - RAND_INT2 / 2 for i in np.arange(0,np.size(dataset_B))])
data = np.array([dataset_A, dataset_B, dataset_C]).transpose()

if isPCA == True:
    df = pd.DataFrame(data=data, columns=['$\mathrm{PC}_1$', '$\mathrm{PC}_2$', '$\mathrm{PC}_3$'])
else:
    df = pd.DataFrame(data=data, columns=['A', 'B', 'C'])
#df_normalized = preprocessing.normalize(df) 

#if isPCA == True:
#    pca = PCA(n_components=3)
#    data_pca = pca.fit_transform(df_normalized)
#
#    df = pd.DataFrame(data_pca, columns=[r'$\mathrm{PC}_1$', r'$\mathrm{PC}_2$', r'$\mathrm{PC}_3$'])



plots = scatter_matrix(df, alpha=0.8, figsize=(4, 4), diagonal='kde')
for i, axs in enumerate(plots):
    for j, ax in enumerate(axs):
        if i != j:  # only the scatter plots
            ax.set_xlim(-1,1)
            ax.set_ylim(-1,1)
        else:
            ax.set_xlim(-1,1)
            #ax.set_ylim(-1,1)
plt.tight_layout(pad=0,rect=[0.05, 0.05, 0.95, 0.95])

if isPCA == True:
    plt.savefig('pca.pdf')
else:
    plt.savefig('scatterplot_matrix_kde.pdf')



# Grid
#plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
#plt.savefig('pca.pdf')
