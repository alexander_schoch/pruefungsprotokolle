# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry
from mpl_toolkits.axisartist.axislines import SubplotZero
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)
#plt.style.use('dark_background')

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

#fig, ax = plt.subplots()
fig = plt.figure()
ax = SubplotZero(fig, 111)
fig.add_subplot(ax)
# set the x-spine (see below for more info on `set_position`)
#ax.spines['left'].set_position('zero')

# turn off the right spine/ticks
#ax.spines['right'].set_color('none')
#ax.spines['top'].set_color('none')

for direction in ['xzero', 'yzero']:
    ax.axis[direction].set_axisline_style('-|>')
    ax.axis[direction].set_visible(True)
    ax.axis[direction].major_ticklabels.set_visible(False)
    ax.axis[direction].set_axislabel_direction("+")

ax.axis['xzero'].label.set_text(r"$x$")
ax.axis['yzero'].label.set_text(r"$T$")

for direction in ['left', 'right', 'bottom', 'top']:
    ax.axis[direction].set_visible(False)

plt.tick_params(
    axis='both',       # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    left=False,
    right=False,
    labelbottom=False,
    labelleft=False) # labels along the bottom edge are off


def func(x):
    return x * np.exp(-3 * x) + 0.05

xarr = np.linspace(0,1,100)
plt.plot(xarr, func(xarr), linewidth=1, color='black')
plt.scatter(1,0,alpha=0)
# Axis labels
#plt.xlabel(r'$t$', fontsize=16)
plt.ylabel(r'a', fontsize=16)

# Grid
#plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.tight_layout()
plt.savefig('TD_with_right.pdf')
