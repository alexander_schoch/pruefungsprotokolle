# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.stats import t
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

def vle(x, a, k):
    return k * x * np.exp(a * (1 - x)**1)

x = np.linspace(0,1,100)

plt.plot(x, vle(x, 1, 1), linewidth=1, color='black', label=r'VLE')
plt.plot([0,1],[0,1],linewidth=1,color='black', linestyle='--', label=r'$x_i = y_i$')

# Axis labels
plt.xlabel(r'$x_i$', fontsize=16)
plt.ylabel(r'$y_i$', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.legend()
plt.tight_layout()
plt.savefig('vle_normal.pdf')
