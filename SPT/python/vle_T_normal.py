# Import libraries: numpy for advanced math stuff, matplotlib.pyplot for creating fancy plots
import numpy as np
import matplotlib.pyplot as plt
import statistics as stat
from scipy.optimize import curve_fit
from scipy.optimize import brentq
from scipy.stats import t
from pint import UnitRegistry
ureg = UnitRegistry(autoconvert_offset_to_baseunit = True)

# Latex implementation: Process all text with latex
plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=r'\usepackage{siunitx}')

def vle(x, a, k):
    return k * x * np.exp(a * (1 - x)**1)

def antoine(theta, a, b, c):
    return 10**(a - b / (c + theta))

def bubblepoint(theta, xi):
    return antoine(theta, 5.37, 1670, -40.2) * xi  +  antoine(theta, 5.4, 1838, -31.7) * (1 - xi) - 1

def dewpoint(theta, xi):
    return xi / antoine(theta, 5.37, 1670, -40.2)  +  (1 - xi) / antoine(theta, 5.4, 1838, -31.7) - 1

x = np.linspace(0,1,100)

bubble = np.array([brentq(bubblepoint, 273, 373, args=(xi)) for xi in x]) - 273.15
dew = np.array([brentq(dewpoint, 273, 373, args=(xi)) for xi in x]) - 273.15


#plt.plot(x, vle(x, 1, 1), linewidth=1, color='black', label=r'VLE')
plt.plot(x, bubble, linewidth=1, color='black', label=r'bubble line')
plt.plot(x, dew, linewidth=1, color='black', label=r'dew line', linestyle='--')
plt.plot(1 - x, bubble, linewidth=1, color='red')
plt.plot(1 - x, dew, linewidth=1, color='red', linestyle='--')


# Axis labels
plt.xlabel(r'$x_i, y_i$', fontsize=16)
plt.ylabel(r'$\theta$ / \si{\celsius}', fontsize=16)

# Grid
plt.grid(color='gray',which='both',linestyle=':',linewidth=0.1)

# make margins nice and print the plot to a pdf file
plt.legend()
plt.tight_layout()
plt.savefig('vle_T_normal.pdf')
